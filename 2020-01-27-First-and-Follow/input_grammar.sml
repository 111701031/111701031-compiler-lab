(* This file contains the grammar of which the first and follow has to be found.
   Any valid description of a grammar can be copied here and the program be runned. *)

use "type.sml";

(* Set of atoms(variables)to represent the symbols in the grammar. *)
val variable = ref AtomSet.empty;
variable := AtomSet.addList (!variable, [Atom.atom "E'", Atom.atom "E", Atom.atom "T", Atom.atom "T'", Atom.atom "F"]);


(* Set of atoms to represent the tokens(terminals) in the grammar. *)
val terminal = ref AtomSet.empty;
terminal := AtomSet.addList (!terminal, [Atom.atom "+", Atom.atom "*", Atom.atom "(", Atom.atom ")", Atom.atom "$", Atom.atom "id"]);


(* Rules corresponding to each symbol in the grammar - each of type Productions.
   First an empty set corresponding to each symbol in the grammar is created and then each production corresponding to the symbol is 
   added successively. *)
val E_prod = ref RHSSet.empty;
E_prod := RHSSet.add (!E_prod, [Atom.atom "T", Atom.atom "E'"]);

val Eprod = ref RHSSet.empty;
Eprod := RHSSet.add (!Eprod, [Atom.atom "+", Atom.atom "T", Atom.atom "E'"]);
Eprod := RHSSet.add (!Eprod, [Atom.atom "EPS"]);

val Fprod = ref RHSSet.empty;
Fprod := RHSSet.add (!Fprod, [Atom.atom "id"]);
Fprod := RHSSet.add (!Fprod, [Atom.atom "(", Atom.atom "E", Atom.atom ")"]);


val T_prod = ref RHSSet.empty;
T_prod := RHSSet.add (!T_prod, [Atom.atom "F", Atom.atom "T'"]);

val Tprod = ref RHSSet.empty;
Tprod := RHSSet.add (!Tprod, [Atom.atom "EPS"]);
Tprod := RHSSet.add (!Tprod, [Atom.atom "*", Atom.atom "F", Atom.atom "T'"]);


(* Rules corresponding to all the symbols - of type Rules.
   First an empty map is created and then the rules for all the symbols are added successively. *)
(*Insert the above production as a rule in the Atom structure*)

val rule : Rules ref = ref AtomMap.empty;
rule := AtomMap.insert (!rule, Atom.atom "E", !E_prod);
rule := AtomMap.insert (!rule, Atom.atom "E'", !Eprod);
rule := AtomMap.insert (!rule, Atom.atom "T", !T_prod);
rule := AtomMap.insert (!rule, Atom.atom "T'", !Tprod);
rule := AtomMap.insert (!rule, Atom.atom "F", !Fprod);


(* Finally all the three components of the grammar - symbols, tokens and rules - defined above are combined in the record grammar
   (of type grammar). *)
val grammar : Grammar = { symbols = !variable, tokens = !terminal, rules = !rule };
