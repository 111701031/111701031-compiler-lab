val c1= "\027[30m"
val c2="\027[31m"
val c3= "\027[32m" 
val c4= "\027[33m"
val c5= "\027[34m"
val c6= "\027[37m" 
val c7= "\027[32;1;3m"
val c8= "\027[33;1m"
val c9= "\027[31;1m" 
val c0= "\027[0m"	

structure Indent = struct
	
	 fun  move s = if (s = 0) then (" ") else ("  "^(move (s-1)))
	 
	fun Indentation space (Ast.E x ) = Indentexpr space x 
	  | Indentation  space (Ast.D y )= Indentdeclist space y

	and

	   Indentdeclist space []       = ""
	  | Indentdeclist space (x::xs)  = (Indentdec space x )^"\n"^(Indentdeclist space  xs)

	and
	 Indentexpr space  (Ast.Const x) =c4^Int.toString x^c0
	  | Indentexpr space  (Ast.IDEN x)   = (move space) ^x
	  | Indentexpr space  (Ast.Quote x) =(move space)^ x
	  | Indentexpr space  (Ast.Op(x,y,z)) = c1^"(" ^c0^ (Indentexpr 0 x) ^c3^ (Ast.binOpToString y) ^c0^ (Indentexpr 0 z) ^c1^ ")"^c0
	  | Indentexpr space  (Ast.Assign(x,y)) = (Indentexpr space x)^c3^" := "^c0^(Indentexpr space y)
	  | Indentexpr space  (Ast.If(x,y )) = c8^" if " ^c0^(Indentexpr space x)^"\n" ^(move (space+3))^c8^" then " ^c0^ (Indentexpr space y)^"\n"
	  | Indentexpr space (Ast.Ifelse(x,y,z)) = c8^" if "^c0^(Indentexpr space x) ^"\n" ^(move (space+3))^c8^" then " ^c0^ (Indentexpr space y) ^"\n" ^(move (space+3))^c8^" else "^c0^(Indentexpr space z)^"\n"
	  | Indentexpr space (Ast.Whiledo(x,y)) = c8^" while "^c0^(Indentexpr space x) ^c8^" do \n"^c0^(Indentexpr (space+2) y)^"\n"
	  | Indentexpr space (Ast.For(x,y,z))  = c8^" for "^c0^(Indentexpr space x)^c8^" to "^c0^(Indentexpr space y)^c8^" do \n"^c0^(Indentexpr (space+2) z)^"\n" 
	  | Indentexpr space (Ast.Break ) 	  = (move space)^"break"^"\n"
	  | Indentexpr space (Ast.Let(x,y))   = c8^" let " ^c0^"\n" ^(Indentdeclist (space+1) x) ^c8^" in " ^"\n" ^c0^(Indentexprlist (space+1) y)^"\n"^c8^" end "^"\n"
	  | Indentexpr space (Ast.NIL     )   = "nil"
	  | Indentexpr space (Ast.Array(x,y,z)) = x^c3^"["^c0^( Indentexpr 0 y)^c3^"]"^c2^" of "^c0^( Indentexpr 0 z)
	  | Indentexpr space (Ast.Recorde(x,y))   = let
						fun printRecbody [] 	= "" 
						|printRecbody ((x,y)::[]) = x^c3^" ="^c0^( Indentexpr 0 y)
						|printRecbody ((x,y)::xs )  = x^c3^" = "^c0^( Indentexpr 0 y)^c3^", "^c0^(printRecbody xs)		
					in
						x^c3^" { "^c0^(printRecbody y)^c3^"} \n"	
					end	
	  | Indentexpr space (Ast.Object(x))   = (move space)^"new "^x
	  | Indentexpr space (Ast.Method(x,y))   = ( Indentexpr space x)^c3^"."^c0^y	
	  | Indentexpr space (Ast.Access(x,y))   = ( Indentexpr 0 x)^"["^( Indentexpr 0 y)^"]"
	  | Indentexpr space (Ast.Funcall(x,y))   = (move space)^x^c3^"( "^c0^(Parguments  y)^c3^" )"^c0
	  | Indentexpr space (Ast.Methodcall(x,y,z))   =  (move space)^(Indentexpr space x)^"."^y^"("^(Parguments  z)^")"
	  | Indentexpr space (Ast.Neg x)   =  "( ~"^( Indentexpr 0 x)^")"
	  | Indentexpr space (Ast.Closed x)   =  "(\n"^(Indentexprlist (space+1) x)^"\n"^(move space)^")"
	  
	and
	Parguments [] = ""
	|Parguments (x::[]) = ( Indentexpr 0 x)^""
	|Parguments (x::xs) = ( Indentexpr 0 x)^", "^(Parguments xs)

	and 
	  Indentexprlist space []          = ""
	  |Indentexprlist space (x::xs)   = (Indentexpr space x) ^"\n"^ (Indentexprlist space xs)    
	  
	 and
	 Indentdec space (Ast.Typedec(x,y))= (move space) ^c2^" type "^c0^ x ^c3^" = "^c0^(Indentty space y)
	 |Indentdec space (Ast.Fundec(x,y,z))= (move space) ^c2^" function "^c0^ x ^c3^ " ( " ^c0^(Indenttyfield space y) ^c3^ " ) " ^ " =" ^ c0^(Indentexpr space z)
	 |Indentdec space (Ast.Fundect(x,y,w,z))= (move space) ^c2^" function "^c0^ x ^c3^ " ( " ^c0^(Indenttyfield space y) ^c3^ " ) "^c3^" : " ^c0^w^c3^ " =" ^c0^ (Indentexpr space z)
	 |Indentdec space (Ast.Primi(x,y))= (move space) ^" primitive "^ x ^ " ( " ^(Indenttyfield space y) ^ " ) "     
	 |Indentdec space (Ast.Primit(x,y,z))= (move space) ^" primitive "^ x ^ " ( " ^(Indenttyfield space y) ^ " ) " ^ " : " ^z
	 |Indentdec space (Ast.Import x)     = (move space) ^c2^ " import " ^c0^ x
	 |Indentdec space (Ast.Class (x,y))  = (move space) ^c2^" class " ^c0^x ^c3^" { " ^c0^(Indentclassf space y) ^c3^" }"^c0 
	 |Indentdec space (Ast.Classe (x,y,z)) =(move space) ^c2^" class " ^c0^x ^c2^"extendes" ^c0^y ^c3^" { " ^c0^(Indentclassf space z) ^c3^" }"
	 |Indentdec space (Ast.Vardec x)      = (move space) ^ (Indentvar space x)
	 
	 and
	  Indentty space (Ast.Ti x)     = x
	 |Indentty space (Ast.Record x) = c3^" { " ^c0^(Indenttyfield space x) ^c3^" } "
	 |Indentty space (Ast.Arrayty x)= c2^" array " ^" of " ^c0^x
	 |Indentty space (Ast.Classty x)= " class " ^" { " ^(Indentclassf space x) ^" } "
	 |Indentty space (Ast.Classtyt (x,y)) = " class " ^" extends " ^x ^" { " ^(Indentclassf space y) ^" } "
	 
	 
	 and
	 Indenttyfield space (Ast.Tyf []      )= ""
	 |Indenttyfield space (Ast.Tyf ((a,b)::[]) )= a ^c3^" : " ^c0^b
	 |Indenttyfield space (Ast.Tyf ((a,b)::xs) ) = a ^c3^" : " ^c0^b ^c3^","^c0^(Indenttyfield space (Ast.Tyf xs)) 
	 
	 and
	  Indentclass space (Ast.Attribute x)  = (move space) ^(Indentvar space x)
	 |Indentclass space (Ast.Meth (x,y,z)) = (move space) ^c2^" method " ^c0^x ^c3^" ( " ^c0^(Indenttyfield space y) ^c3^" ) " ^" = " ^c0^(Indentexpr space z)
	 |Indentclass space (Ast.Metht (x,y,z,w)) = (move space) ^c2^" method " ^c0^x ^c3^" ( " ^c0^(Indenttyfield space y) ^" ) " ^c3^" : " ^z ^" = " ^c0^(Indentexpr space w)
	  
	 and
	 Indentclassf space [] = ""
	 |Indentclassf space (x::xs) = (Indentclass space x )^"\n"^(Indentclassf space  xs)
	 
	 and 
	 Indentvar space (Ast.Var (x,y))    = c2^" var " ^c0^x ^c3^" := "  ^c0^(Indentexpr space y)
	 |Indentvar space (Ast.Vart (x,y,z)) = c2^" var " ^c0^x ^c3^" : " ^c0^y ^c3^" := "  ^c0^(Indentexpr space z)
end
