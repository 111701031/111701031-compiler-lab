datatype Token = num of int 
		| PLUS 
		| TAB
		| SPACE
		| MINUS 
		| MUL
		| DIV
		| Comment of string
		| Identifier of string
		| KEYWORD of string
		| SYMBOL of string
		| Newline
	        | EOF
	     
			
