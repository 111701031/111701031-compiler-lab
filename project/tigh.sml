	fun colour x = (case x of
		    0 => "\027[30m"
		 | 1  => "\027[31m"
		 | 2  => "\027[32m" 
		 | 3  => "\027[33m"
		 | 4  => "\027[34m"
		 | 5  => "\027[37m" 
		 | 6  => "\027[32;1;3m"
		 | 7  => "\027[33;1m"
		 | 8  => "\027[31;1m" 
	         | _  => "\027[0m"	 
)

   
