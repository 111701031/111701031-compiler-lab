fun tostring x = Int.toString x

fun Print ( num x )             = print( (colour 0) ^ tostring x)
  | Print PLUS                  = print( (colour 1) ^ "+")
  | Print MINUS                 = print( (colour 2) ^ "-")
  | Print MUL                   = print( (colour 3) ^ "*")
  | Print DIV                   = print( (colour 4) ^ "/")
  | Print (Identifier x)        = print( (colour 5) ^ x)
  | Print (Comment x)           = print( (colour 6) ^ x ^ "\027[0m")  
  | Print Newline               = print("\n")
  | Print TAB                   = print("\t")
  | Print SPACE                 = print(" ")
  | Print (KEYWORD x)           = print( (colour 7) ^ x)
  | Print (SYMBOL  x)           = print( ( colour 8) ^ x)
  | Print _ 			= print("\n")  

