structure Ast = struct
datatype Expr = Const of int
	   | Op of Expr * Binop * Expr
	   | IDEN of string
	   | Assign of Expr * Expr
	   | If of Expr * Expr
	   | Ifelse of Expr * Expr * Expr
	   | Whiledo of Expr * Expr
           | For of Expr * Expr * Expr
  	   | Break 
	   | Let of (Dec list) * (Expr list)
	   | NIL
	   | Neg of Expr
	   | Closed of Expr list
	   | Method of Expr * string
	   | Access of Expr * Expr
	   | Recorde of string * (string * Expr) list
	   | Array of string * Expr * Expr
	   | Object of string
	   | Funcall of string * Expr list
	   | Methodcall of Expr * string * Expr list
	   | Quote of string
  and Dec  = Typedec of string*Ty 
	    | Fundec of string*Tyfield*Expr
            | Fundect of string*Tyfield*string*Expr
	    | Primi of  string*Tyfield
	    | Primit of  string*Tyfield*string
            | Import of string
            | Class  of string * (Classfield list)
            | Classe of string * string * (Classfield list)
            | Vardec of Vdec
 and Vdec  = Var of string * Expr | Vart of string * string * Expr
 and Classfield = Attribute of Vdec | Meth of string * Tyfield * Expr | Metht of string * Tyfield * string * Expr
 and Tyfield = Tyf of (string * string) list
 and Ty      = Ti of string | Record of Tyfield | Arrayty of string | Classty of (Classfield list) | Classtyt of  string * (Classfield list)
 and Binop = Plus | Minus | Mul | Div | Le | Ge | Eq | Neq | Less | Greater|And| Or

datatype Prog = E of Expr | D of (Dec list)

(* fun binOpDenote Plus x y = x + y
  | binOpDenote Minus x y = x - y
  | binOpDenote Mul   x y = x * y
  | binOpDenote Div   x y = x div y
  | binOpDenote Le    x y = x <= y
  | binOpDenote Ge    x y = x >= y
  | binOpDenote Eq    x y = x = y
  | binOpDenote Neq   x y = x <> y
  | binOpDenote Less  x y = x < y
fun exprDenote (Const x)        = x
  | exprDenote  (Op (x,oper,y) ) = binOpDenote oper (exprDenote x) (exprDenote y)
;
*)

fun binOpToString  Plus  = "+"
  | binOpToString   Minus = "-"
  | binOpToString   Mul   = "*"
  | binOpToString   Div   = "/"
  | binOpToString   Le   = "<="
  | binOpToString   Ge   = ">="
  | binOpToString   Eq   = "="
  | binOpToString   Neq   = "<>"
  | binOpToString   Less   = "<"
  | binOpToString   Greater   = ">"
  | binOpToString   And     = "&"
  | binOpToString   Or      = "|"

fun idToString (IDEN x) = x
  | idToString _  =   ""
			  

fun plus a b = Op (a, Plus , b)
fun minus a b = Op (a, Minus, b)
fun mul a b = Op (a, Mul, b)
fun divv a b = Op (a,Div,b)
fun leq a b = Op (a,Le,b)
fun geq a b = Op (a,Ge,b)
fun eq  a b = Op (a,Eq,b)
fun neq a b = Op (a,Neq,b)
fun les a b = Op (a,Less,b)
fun gre a b = Op (a,Greater,b)
fun andd a b      = Op (a,And,b)
fun orr  a b      = Op (a,Or,b)
fun assign  a b   = Assign(a,b)
fun iff  a b      = If(a,b)
fun ifelse a b c   = Ifelse(a,b,c)
fun whiledo a b   = Whiledo(a,b)
fun fordo   a b c = For(a,b,c)
end	 
