structure Tiger = 
struct

structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure TigerLex    = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerParser = Join( structure ParserData = TigerLrVals.ParserData
                             structure Lex        = TigerLex
                             structure LrParser   = LrParser
                           )


fun makeExprLexer strm = TigerParser.makeLexer (fn n => TextIO.inputN(strm,n))
val makeFileLexer      = makeExprLexer o TextIO.openIn

val thisLexer = case CommandLine.arguments() of
                    []  => makeExprLexer TextIO.stdIn
                 |  [x] => makeFileLexer x
                 |  _   => (TextIO.output(TextIO.stdErr, "usage: ec file"); OS.Process.exit OS.Process.failure)

fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr,
                                            "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")

val (program,_) = TigerParser.parse (0,thisLexer,print_error,()) (* parsing *)
val executable  = Indent.Indentation 0 program                  (*Indendation*)                 
val _           = TextIO.output(TextIO.stdOut, executable)
                               (* writing out the executable (in this case rp expression ) *)



end



