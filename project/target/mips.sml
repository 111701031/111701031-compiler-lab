signature TEMP = sig
	type temp (* temporary variables of your program *)
	type label (* temporary labels of your program *)
	val newlabel : unit -> label
	val newtemp  : unit -> temp

end

(* manage the generation of temporary variables and labels *)
structure Temp :TEMP = struct
	type temp  = int
	type label = int
	
	val tempcnt = ref 0
	val labelcnt = ref 0

	(* function to add new label and temp *)
			   
	fun newlabel() = (tempcnt := !tempcnt + 1; !tempcnt)
	fun newtemp()   = (labelcnt := !labelcnt + 1; !labelcnt)


end

structure MIPS = struct

	datatype regs = Zero       (* Always 0*)
			| At       (* Reserved for assembler*)
			| Result   (* Stores result *)
			| Argument (* Stores arguments *)
			| Temporary (* Temporaries *)
			| Save     (* Contents saved for later *)
			| Os       (* Reserved by mips operating system *)
			| Gp       (* Global pointer *)
			| Sp       (* Stack Pointer *)
			| Fp       (* Frame pointer *)
			| Ra       (* Return address *)
	and Result   =  V0|V1
	and Argument =  A0 | A1 | A2 | A3
	and Temporary = T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9
	and Os       =  K0 | K1

	 
	(* Converts an instruction where the labels are strings and temps are actual machine registers to a string.This function is used at the end to emit the actual code *)
	datatype ('l , 't) inst =  Add   of 't * 't * 't 
				 | Sub   of 't * 't * 't
				 | Addi  of 't * 't * int
				 | Addu  of 't * 't * 't
				 | Subu  of 't * 't * 't
				 | Addiu of 't * 't * int
				 | Mul   of 't * 't * 't
				 | Mult  of 't * 't
				 | Div   of 't * 't
				 | And   of 't * 't * 't
				 | Or    of 't * 't * 't
				 | Andi  of 't * 't * int
				 | Ori   of 't * 't * int
				 | Sll   of 't * 't * int
				 | Srl   of 't * 't * int
				 | Lw    of 't * int * 't
				 | Sw    of 't * int * 't
				 | Lui   of 't * int
				 | La    of 't * 'l
				 | Li    of 't * int
				 | Mfhi  of 't
				 | Mflo  of 't
				 | Move  of 't * 't
				 | Beq   of 't * 't * int
				 | Bne   of 't * 't * int
				 | Bgt   of 't * 't * int
				 | Bge   of 't * 't * int
				 | Blt   of 't * 't * int
				 | Ble   of 't * 't * int
				 | Slt   of 't * 't * 't
				 | Slti  of 't * 't * int
				 | J     of 'l
				 | Jr    of 't
				 | Jal   of 'l
				 | Syscalls

	fun f x  = Int.toString x

	fun pretty i = ( case i of 
			Add(a,b,c) => print ("add" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c) ^ "\n")
			  |  Sub(a,b,c) => print ("sub" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			  |  Addi(a,b,c) => print ("addi" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			  |  Addu(a,b,c) => print ("addu" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			  |  Subu(a,b,c) => print ("subu" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			|  Addiu(a,b,c) => print ("addiu"^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Mul(a,b,c) =>  print ("mul" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			|  Mult(a,b) =>  print ("mult" ^ " $" ^ (f a) ^ ",$"^(f b) ^"\n")
			|  Div(a,b) => print ("div" ^ " $" ^ (f a) ^ ",$"^(f b) ^"\n")
			|  And(a,b,c) => print ("and" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			|  Or(a,b,c) => print ("or" ^ " $" ^ (f a) ^ ",$"^(f b) ^",$"^(f c)^"\n")
			|  Andi(a,b,c) => print ("andi" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Ori(a,b,c) => print ("ori" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Sll(a,b,c) => print ("sll" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Srl(a,b,c) => print ("srl" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Lw(a,b,c) => print ("lw" ^ " $" ^ (f a)^","^(f b) ^"($"^(f c)^")\n")
			|  Sw(a,b,c) => print ("sw" ^ " $" ^ (f a) ^ ","^(f b) ^"($"^(f c)^")\n")
			|  Lui(a,b) => print ("lui" ^ " $" ^ (f a) ^ ","^(f b) ^"\n")
			|  La(a,b) => print ("la" ^ " $" ^ (f a) ^ ","^(f b) ^"\n")
			|  Li(a,b) => print ("li" ^ " $" ^ (f a) ^ ","^(f b) ^"\n")
			|  Mfhi(a) => print ("mfhi" ^ " $" ^ (f a) ^"\n")
			|  Mflo(a) => print ("mflo" ^ " $" ^ (f a) ^"\n")
			|  Move(a,b) => print ("move" ^ " $" ^ (f a) ^ ",$"^(f b) ^"\n")
			|  Beq(a,b,c) => print ("beq" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Bne(a,b,c) => print ("bne" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Bgt(a,b,c) => print ("bgt" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Bge(a,b,c) => print ("bge" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Blt(a,b,c) => print ("blt" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Ble(a,b,c) => print ("ble" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Slti(a,b,c) => print ("slti" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  Slt(a,b,c) => print ("slt" ^ " $" ^ (f a) ^ ",$"^(f b) ^","^(f c)^"\n")
			|  J(a) => print ("j " ^(f a)^ "\n")
			|  Jr(a) => print ("jr " ^"$"^(f a)^ "\n")
			|  Jal(a) => print ("jal " ^"$"^(f a)^ "\n")
			|  Syscalls => print("syscall\n")

)


end

(*

	Example_testcase
	------------------------

	- val r0 = Temp.newtemp();
	val r0 = 1 : Temp.temp
	- val r1 = Temp.newtemp();
	val r1 = 2 : Temp.temp
	- val r2 = Temp.newtemp();
	val r2 = 3 : Temp.temp
	- val t = MIPS.Add(r0,r1,r2);
	val t = Add (1,2,3) : ('a,Temp.temp) MIPS.inst
	- MIPS.pretty(t);  
	add $1,$2,$3
	val it = () : unit



*)
